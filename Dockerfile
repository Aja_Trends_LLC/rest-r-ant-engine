From openjdk:11-jdk-stretch as build
#VOLUME /tmp
WORKDIR /workspace/app
COPY gradlew .
COPY gradle gradle
COPY build.gradle .
COPY settings.gradle .
COPY src src
COPY config config

RUN ./gradlew build
RUN mkdir -p build/dependency && (cd build/dependency; jar -xf ../libs/*.jar)
#RUN find build/dependency

From openjdk:11-jdk-stretch
VOLUME /tmp
ARG server_port=8080

ENV PORT=$server_port

#ENV JAVA_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8001"

COPY --from=0 /workspace/app/build/dependency/BOOT-INF/lib /app/lib
COPY --from=0 /workspace/app/build/dependency/META-INF /app/META-INF
COPY --from=0 /workspace/app/build/dependency/BOOT-INF/classes /app

RUN echo $PORT

EXPOSE 8080

#CMD ["java","-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8001","-cp", "app:app/lib/*", "com.aja.restrant.RestRantApplication"]
CMD java $JAVA_OPTS -cp app:app/lib/* -Dserver.port=$PORT com.aja.restrant.RestRantApplication

