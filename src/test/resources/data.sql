INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE') , 'A medley of mixed vegetables dipped in chick peas and rice flour batter and fried. Vegetarian Test.', 'Veg Pakoda Test', '10.00'
   )
;
INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE') , 'Deep fried lentil donuts. Vegetarian. Test', 'Vada Test', '5.00'
   )
;
INSERT INTO
   MENU_CATEGORY
VALUES
   (
      nextval('HIBERNATE_SEQUENCE') , 'Appetizers Test', 'Appetizers Test', 1
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE') , 1,
      select
         id
      from
         MENU_CATEGORY
      where
         name = 'Appetizers Test',
         select
            id
         from
            MENU_ITEM
         where
            name = 'Veg Pakoda Test'
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      1,
      select
         id
      from
         MENU_CATEGORY
      where
         name = 'Appetizers Test',
         select
            id
         from
            MENU_ITEM
         where
            name = 'Vada Test'
   )
;
INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Chicken cooked butter in a light creamy sauce Test.',
      'Butter Chicken Test',
      '13.50'
   )
;
INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Homemade cheese cooked with fresh spinach spices and cream. Vegetarian Test.',
      'Palak Paneer Test',
      '11.00'
   )
;
INSERT INTO
   MENU_CATEGORY
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Entrees Test',
      'Entrees Test',
      2
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      1,
      select
         id
      from
         MENU_CATEGORY
      where
         name = 'Entrees Test',
         select
            id
         from
            MENU_ITEM
         where
            name = 'Butter Chicken Test'
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      1,
      select
         id
      from
         MENU_CATEGORY
      where
         name = 'Entrees Test',
         select
            id
         from
            MENU_ITEM
         where
            name = 'Palak Paneer Test'
   )
;
INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Homemade whisked yogurt shake Test.',
      'Hangover Buttermilk Test',
      '4.00'
   )
;
INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Mango and yogurt drink Test.',
      'Mango Lassi Test',
      '3.00'
   )
;
INSERT INTO
   MENU_CATEGORY
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Drinks Test',
      'Drinks Test',
      3
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      1,
      select
         id
      from
         MENU_CATEGORY
      where
         name = 'Drinks Test',
         select
            id
         from
            MENU_ITEM
         where
            name = 'Hangover Buttermilk Test'
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      1,
      select
         id
      from
         MENU_CATEGORY
      where
         name = 'Drinks Test',
         select
            id
         from
            MENU_ITEM
         where
            name = 'Mango Lassi Test'
   )
;