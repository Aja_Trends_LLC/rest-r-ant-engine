package com.aja.restrant.menu.gateway;

import com.aja.restrant.menu.domain.entity.MenuCategory;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MenuRepositoryImplTest {

  @Autowired
  private MenuRepositoryImpl menuRepository;

  @Test
  public void testGetMenu() {
    List<MenuCategory> menuCategories = menuRepository.getMenu();
    Assert.assertFalse(menuCategories.isEmpty());
  }

}
