package com.aja.restrant.menu.gateway;

import static org.assertj.core.api.Assertions.assertThat;

import com.aja.restrant.menu.gateway.repoobjects.MenuCategoryRO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MenuCategoryRepoTest {

  @Autowired
  private MenuCategoryRepo menuCategoryRepo;

  @Test
  public void testMenuCategory_findAll() {
    Iterable<MenuCategoryRO> menuCategory = menuCategoryRepo.findAll();
    assertThat(menuCategory.iterator().hasNext()).isEqualTo(true);
  }

  @Test
  public void testMenuCategory_save() {
    MenuCategoryRO categoryRO = getMenuCategory();
    menuCategoryRepo.save(categoryRO);

    MenuCategoryRO menuCategoryRO =  menuCategoryRepo.findByCategoryName("Soups Test");
    assertThat(menuCategoryRO.getCategoryName()).isEqualTo(categoryRO.getCategoryName());
  }

  private MenuCategoryRO getMenuCategory() {
    MenuCategoryRO categoryRO = new MenuCategoryRO();
    categoryRO.setCategoryName("Soups Test");
    categoryRO.setCategoryDesc("Soups Desc Test");
    categoryRO.setRank(1);
    return categoryRO;
  }

}
