package com.aja.restrant.menu.gateway;

import static org.assertj.core.api.Assertions.assertThat;

import com.aja.restrant.menu.gateway.repoobjects.MenuItemRO;
import java.math.BigDecimal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MenuItemRepoTest {

  @Autowired
  private MenuItemRepo menuItemRepo;

  @Test
  public void testMenuItem_findAll() {
    Iterable<MenuItemRO> items = menuItemRepo.findAll();
    assertThat(items.iterator().hasNext()).isEqualTo(true);
  }

  @Test
  public void testMenuItem_save() {
    MenuItemRO menuItemRO = getMenuItem("Chicken", "Chicken Desc", BigDecimal.valueOf(12));
    menuItemRepo.save(menuItemRO);

    MenuItemRO itemRO = menuItemRepo.findByItemName("Chicken");
    assertThat(itemRO.getItemName()).isEqualTo(menuItemRO.getItemName());
  }

  @Test
  public void testMenuItem_savePrice() {
    MenuItemRO menuItemRO = getMenuItem("Idli Test", "Idli Desc Test", BigDecimal.valueOf(12));
    menuItemRepo.save(menuItemRO);

    MenuItemRO itemRO = menuItemRepo.findByItemName("Idli Test");
    assertThat(itemRO.getItemPrice()).isEqualTo(menuItemRO.getItemPrice());
  }

  private MenuItemRO getMenuItem(String name, String desc, BigDecimal price) {
    MenuItemRO menuItemRO = new MenuItemRO();
    menuItemRO.setItemName(name);
    menuItemRO.setItemDesc(desc);
    menuItemRO.setItemPrice(price);
    return menuItemRO;
  }
}
