package com.aja.restrant.menu.gateway;

import static org.junit.Assert.assertTrue;

import com.aja.restrant.menu.gateway.repoobjects.MenuCategoryItemRO;
import com.aja.restrant.menu.gateway.repoobjects.MenuCategoryRO;
import com.aja.restrant.menu.gateway.repoobjects.MenuItemRO;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MenuCategoryItemRepoTest {

  @Autowired
  private MenuCategoryItemRepo menuCategoryItemRepo;

  @Test
  public void testMenuCatItem_findAll() {
    Iterable<MenuCategoryItemRO> item = menuCategoryItemRepo.findAll();
    assertTrue(item.iterator().hasNext());
  }

  @Test
  public void testMenuCatItem_findCat() {
    Iterable<MenuCategoryItemRO> item = menuCategoryItemRepo.findAll();
    List<MenuCategoryRO> menuCategoryROs = new ArrayList<>();
    item.forEach(
        menuCategoryItemRO -> menuCategoryROs.add(menuCategoryItemRO.getMenuCategory())
    );
    assertTrue(menuCategoryROs.size() > 1);
  }

  @Test
  public void testMenuCatItem_findMenuItem() {
    Iterable<MenuCategoryItemRO> item = menuCategoryItemRepo.findAll();
    List<MenuItemRO> menuItemROs = new ArrayList<>();
    item.forEach(
        menuCategoryItemRO -> menuItemROs.add(menuCategoryItemRO.getMenuItem())
    );
    assertTrue(menuItemROs.size() > 1);
  }

}