package com.aja.restrant.menu.presenter;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.aja.restrant.menu.domain.entity.MenuCategory;
import com.aja.restrant.menu.domain.entity.MenuItem;
import com.aja.restrant.menu.domain.entity.NameAndDesc;
import com.aja.restrant.menu.presenter.model.MenuCategoryVM;
import com.aja.restrant.menu.presenter.model.MenuItemVM;
import com.aja.restrant.menu.presenter.model.MenuMV;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@Import(TestConfig.class)
public class MenuPresenterImplTest {

  @Autowired
  private HttpServletRequest httpServletRequest;
  @Autowired
  private MenuPresenterImpl menuPresenter2;



  @Test
  public void testShowMenuModel() {
    menuPresenter2.showMenu(getMenuCategories());
    MenuMV menuMV = (MenuMV)httpServletRequest.getAttribute("response");
    assertNotNull(menuMV);
  }


  @Test
  public void testShowMenuCategories() {
    menuPresenter2.showMenu(getMenuCategories());
    MenuMV menuMV = (MenuMV)httpServletRequest.getAttribute("response");
    assertNotNull(menuMV.getCategories());
  }

  @Test
  public void testShowMenuCategory() {
    menuPresenter2.showMenu(getMenuCategories());
    MenuMV menuMV = (MenuMV)httpServletRequest.getAttribute("response");
    MenuCategoryVM menuCategoryVM = menuMV.getCategories().get(0);
    assertEquals("Soups", menuCategoryVM.getCategoryName());
  }

  @Test
  public void testShowMenuItems() {
    menuPresenter2.showMenu(getMenuCategories());
    MenuMV menuMV = (MenuMV)httpServletRequest.getAttribute("response");
    assertEquals(1, menuMV.getCategories().size());
  }

  @Test
  public void testShowMenuItem() {
    menuPresenter2.showMenu(getMenuCategories());
    MenuMV menuMV = (MenuMV)httpServletRequest.getAttribute("response");
    MenuCategoryVM menuCategoryVM = menuMV.getCategories().get(0);
    MenuItemVM menuItemVM = menuCategoryVM.getMenuItems().get(0);
    assertEquals("Chicken Noodle", menuItemVM.getItemName());
  }

  private List<MenuCategory> getMenuCategories() {
    List<MenuItem> menuItems = getMenuItems();
    MenuCategory menuCategory = getMenuCategory();
    menuCategory.setMenuItems(menuItems);
    List<MenuCategory> menuCategories = asList(menuCategory);
    return menuCategories;
  }

  private List<MenuItem> getMenuItems() {
    MenuItem menuItem = new MenuItem();
    menuItem.setNameAndDesc(getNameAndDesc("Chicken Noodle", "Chicken Noodle Desc"));
    menuItem.setRank(1);
    List<MenuItem> menuItems = asList(menuItem);
    return menuItems;
  }

  private NameAndDesc getNameAndDesc(String name, String desc) {
    NameAndDesc nameAndDesc = new NameAndDesc();
    nameAndDesc.setName(name);
    nameAndDesc.setDesc(desc);
    return nameAndDesc;
  }

  private MenuCategory getMenuCategory() {
    MenuCategory menuCategory = new MenuCategory();
    menuCategory.setRank(1);
    menuCategory.setNameAndDesc(getNameAndDesc("Soups", "Soups Desc"));
    return menuCategory;
  }
}
