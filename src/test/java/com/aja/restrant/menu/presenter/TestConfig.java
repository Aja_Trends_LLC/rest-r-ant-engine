package com.aja.restrant.menu.presenter;

import com.aja.restrant.menu.domain.usecase.MenuPresenter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.web.MockHttpServletRequest;

@TestConfiguration
public class TestConfig {
  @Bean
  public HttpServletRequest httpServletRequest() {
    return  new MockHttpServletRequest();
  }

  @Bean
  public MenuPresenter menuPresenter2() {
    return new MenuPresenterImpl();
  }

}
