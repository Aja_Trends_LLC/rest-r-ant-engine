package com.aja.restrant.menu.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.aja.restrant.menu.domain.entity.MenuCategory;
import com.aja.restrant.menu.domain.entity.MenuItem;
import com.aja.restrant.menu.domain.entity.NameAndDesc;
import com.aja.restrant.menu.domain.usecase.GetMenuInteractorImpl;
import com.aja.restrant.menu.domain.usecase.MenuPresenter;
import com.aja.restrant.menu.domain.usecase.MenuRepository;
import com.aja.restrant.menu.presenter.model.MenuCategoryVM;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class GetMenuInteractorImplTest {

  private GetMenuInteractorImpl getMenuInteractorImpl;

  private MenuRepository menuRepository;

  private MenuPresenter menuPresenter;

  private List<MenuCategoryVM> responseMenuCategoryVMS;

  @Before
  public void setup() {
    List<MenuCategory> menuCategories = getMenuCategories();
    menuRepository = () -> (menuCategories);

    List<MenuCategoryVM> menuCategoryVMS = getMenuCategorysVM();
    menuPresenter = (menuCategories2) -> responseMenuCategoryVMS = menuCategoryVMS;
    getMenuInteractorImpl = new GetMenuInteractorImpl(menuRepository, menuPresenter);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testGetMenu() {
   getMenuInteractorImpl.process();
    assertThat(responseMenuCategoryVMS.size() == 1).isEqualTo(true);
  }

  private List<MenuCategory> getMenuCategories() {
    List<MenuCategory> menuCategories = new ArrayList<>();
    List<MenuItem> menuItems = getMenuItems();
    MenuCategory menuCategory = getMenuCategory();
    menuCategory.setMenuItems(menuItems);
    menuCategories.add(menuCategory);
    return menuCategories;
  }

  private List<MenuItem> getMenuItems() {
    MenuItem menuItem = new MenuItem();
    menuItem.setNameAndDesc(getNameAndDesc("chickenNoodle", "Chicken Noodle"));
    menuItem.setRank(1);
    menuItem.setPrice(BigDecimal.TEN);
    List<MenuItem> menuItems = new ArrayList<>();
    menuItems.add(menuItem);
    return menuItems;
  }

  private NameAndDesc getNameAndDesc(String name, String desc) {
    NameAndDesc nameAndDesc = new NameAndDesc();
    nameAndDesc.setName(name);
    nameAndDesc.setDesc(desc);
    return nameAndDesc;
  }

  private MenuCategory getMenuCategory() {
    MenuCategory menuCategory = new MenuCategory();
    menuCategory.setRank(1);
    menuCategory.setNameAndDesc(getNameAndDesc("soup", "Soup"));
    return menuCategory;
  }

  private List<MenuCategoryVM> getMenuCategorysVM() {
    MenuCategoryVM menuCategoryVM = new MenuCategoryVM();
    menuCategoryVM.setCategoryName("Soup");
    menuCategoryVM.setCategoryDesc("Soup Desc");
    List<MenuCategoryVM> menuCategoryVMs = new ArrayList<>();
    menuCategoryVMs.add(menuCategoryVM);
    return menuCategoryVMs;
  }
}
