package com.aja.restrant;

import com.aja.restrant.menu.gateway.MenuItemRepo;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestRantApplicationTests {

  @MockBean
  private MenuItemRepo menuItemRepo;

  @Test
  public void contextLoads() {
  }

}
