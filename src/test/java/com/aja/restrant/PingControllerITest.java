package com.aja.restrant;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.aja.restrant.menu.gateway.MenuItemRepo;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PingControllerITest {
  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private MenuItemRepo menuItemRepo;

  @Test
  public void testPingDefault() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/ping/").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("Ping: Hi from SpringBoot")));
  }

  @Test
  public void testPingName() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/ping/Amar").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("Ping: Hi Amar!, from SpringBoot, How are you?")));
  }
}
