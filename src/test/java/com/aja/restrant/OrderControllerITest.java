package com.aja.restrant;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.aja.restrant.menu.gateway.MenuItemRepo;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@EnableWebMvc
public class OrderControllerITest {
  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private MenuItemRepo menuItemRepo;

  @Test
  public void testSetOrder() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.post("/setorder/")
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .accept(MediaType.APPLICATION_JSON).content(prepareRequest().toString()))
        .andExpect(status().isOk())
        .andExpect(content().string(notNullValue()));
  }

  private JSONObject prepareRequest() throws Exception {
    JSONObject jsonObjectChild = new JSONObject();
    jsonObjectChild.put("itemId", "123");
    jsonObjectChild.put("numberOfItems", "2");

    JSONArray jsonArray = new JSONArray();
    jsonArray.put(jsonObjectChild);

    JSONObject jsonObject = new JSONObject();
    jsonObject.put("orderItems", jsonArray);
    jsonObject.put("tipAmount", 15D);
    return jsonObject;
  }
}
