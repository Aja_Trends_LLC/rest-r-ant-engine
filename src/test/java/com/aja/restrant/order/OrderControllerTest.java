package com.aja.restrant.order;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.aja.restrant.RestRantApplication;
import com.aja.restrant.menu.presenter.TestConfig;
import com.aja.restrant.order.domain.SubmitOrderInteractor;
import com.aja.restrant.order.domain.entity.Order;
import com.aja.restrant.order.domain.request.OrderItemRequest;
import com.aja.restrant.order.domain.request.OrderRequest;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@Import({TestConfig.class, RestRantApplication.class})
public class OrderControllerTest {

  @Autowired
  private OrderController orderController;
  @Autowired
  private HttpServletRequest httpServletRequest;

  @Mock
  private SubmitOrderInteractor submitOrderInteractor;

  @Before
  public void setUp() {
    orderController.setSubmitOrderInteractor(submitOrderInteractor);
    httpServletRequest.setAttribute("response", "1234");
  }

  @Test(expected = NullPointerException.class)
  public void testConvertToRequestModelNull() throws Exception {
    orderController.setOrder(null);
  }

  @Test
  public void testConvertToRequestModelNoData() throws Exception {
    assertThat(orderController.setOrder(new OrderRequest())).isEqualTo("1234");
  }

  @Test
  public void testConvertToRequestModelWithData() throws Exception {
    httpServletRequest.setAttribute("response", "9909000");
    assertThat(orderController.setOrder(prepareOrderRequest())).isEqualTo("9909000");
  }

  private OrderRequest prepareOrderRequest() {
    OrderRequest orderRequest = new OrderRequest();
    orderRequest.setOrderItems(prepareOrderItems());
    orderRequest.setTipAmount("2");
    return orderRequest;
  }

  private List<OrderItemRequest> prepareOrderItems() {
    List<OrderItemRequest> orderItemRequests = new ArrayList<>();
    OrderItemRequest orderItemRequest = new OrderItemRequest();
    orderItemRequest.setItemId(123L);
    orderItemRequest.setNumberOfItems(2);
    orderItemRequests.add(orderItemRequest);
    return orderItemRequests;
  }

}
