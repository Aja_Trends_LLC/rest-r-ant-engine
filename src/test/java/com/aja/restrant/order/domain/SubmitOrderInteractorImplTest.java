package com.aja.restrant.order.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.aja.restrant.order.domain.entity.Order;
import java.io.IOException;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class SubmitOrderInteractorImplTest {
  private SubmitOrderInteractorImpl orderInteractor;

  private OrderRepository orderRepository;

  private OrderPresenter orderPresenter;


  @Test
  public void testProcess() {
    Order order = new Order();
    StringBuffer confirmationNumber = new StringBuffer();
    orderRepository = (order1) -> ("12345");
    orderPresenter = (confirmationNumber1) -> confirmationNumber.append(confirmationNumber1);
    orderInteractor = new SubmitOrderInteractorImpl(orderRepository,orderPresenter);
    orderInteractor.process(order);
    assertThat(confirmationNumber.toString()).isEqualTo("12345");
  }

}
