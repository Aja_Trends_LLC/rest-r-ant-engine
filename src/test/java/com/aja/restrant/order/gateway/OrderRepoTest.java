package com.aja.restrant.order.gateway;

import static org.assertj.core.api.Assertions.assertThat;

import com.aja.restrant.RestRantApplication;
import com.aja.restrant.order.gateway.repoobjects.OrderItemRO;
import com.aja.restrant.order.gateway.repoobjects.OrderRO;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderRepoTest {

  @Autowired
  private OrderRepo orderRepo;

  @Test
  public void testSaveOrder() {
    OrderRO orderRO = getOrderRO();
    orderRO = orderRepo.save(orderRO);
    assertThat(orderRO.getId()).isNotNull();
  }

  @Test
  public void testFindOrder() {
    orderRepo.save(getOrderRO());
    Iterable<OrderRO> orders = orderRepo.findAll();
    OrderRO orderROtest = orders.iterator().next();
    assertThat(orderROtest.getTipAmount()).isEqualTo(new BigDecimal("2.00"));
  }

  private OrderRO getOrderRO() {
    OrderRO orderRO = new OrderRO();
    OrderItemRO orderItemRO = new OrderItemRO();
    orderItemRO.setItemId(12345L);

    Set<OrderItemRO> orderItemROs = new HashSet<>();
    orderItemROs.add(orderItemRO);

    orderRO.setOrderItems(orderItemROs);
    orderRO.setTipAmount(new BigDecimal("2.00"));
    return orderRO;
  }
}
