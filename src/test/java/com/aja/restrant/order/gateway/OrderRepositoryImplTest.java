package com.aja.restrant.order.gateway;

import static org.assertj.core.api.Assertions.assertThat;

import com.aja.restrant.order.domain.entity.Order;
import com.aja.restrant.order.domain.entity.OrderItem;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderRepositoryImplTest {

  @Autowired
  private OrderRepositoryImpl orderRepository;

  @Test
  public void testSetOrder() {
    OrderItem orderItem = new OrderItem();
    orderItem.setOrderItemId(12345L);
    orderItem.setItemsCount(2);
    List<OrderItem> orderItems = new ArrayList<>();
    orderItems.add(orderItem);

    Order order = new Order();
    order.setItems(orderItems);
    order.setTip("2.00");
    assertThat(orderRepository.setOrder(order)).isNotEmpty();
  }
}
