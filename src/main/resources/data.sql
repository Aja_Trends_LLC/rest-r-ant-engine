INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE') , 'A medley of mixed vegetables dipped in chick peas and rice flour batter and fried. Vegetarian.', 'Veg Pakoda', '10.00'
   )
;
INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE') , 'Deep fried lentil donuts. Vegetarian.', 'Vada', '9.00'
   )
;
INSERT INTO
   MENU_CATEGORY
VALUES
   (
      nextval('HIBERNATE_SEQUENCE') , 'Appetizers', 'Appetizers', 1
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE') , 1,
      (select
         id
      from
         MENU_CATEGORY
      where
         name = 'Appetizers'),
         (select
            id
         from
            MENU_ITEM
         where
            name = 'Veg Pakoda')
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      1,
      (select
         id
      from
         MENU_CATEGORY
      where
         name = 'Appetizers'),
         (select
            id
         from
            MENU_ITEM
         where
            name = 'Vada')
   )
;
INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Chicken cooked butter in a light creamy sauce.',
      'Butter Chicken',
      '12.00'
   )
;
INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Homemade cheese cooked with fresh spinach spices and cream. Vegetarian.',
      'Palak Paneer',
      '10.00'
   )
;
INSERT INTO
   MENU_CATEGORY
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Entrees',
      'Entrees',
      2
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      1,
      (select
         id
      from
         MENU_CATEGORY
      where
         name = 'Entrees'),
         (select
            id
         from
            MENU_ITEM
         where
            name = 'Butter Chicken')
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      1,
      (select
         id
      from
         MENU_CATEGORY
      where
         name = 'Entrees'),
         (select
            id
         from
            MENU_ITEM
         where
            name = 'Palak Paneer')
   )
;
INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Homemade whisked yogurt shake.',
      'Hangover Buttermilk',
      '3.00'
   )
;
INSERT INTO
   MENU_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Mango and yogurt drink.',
      'Mango Lassi',
      '4.00'
   )
;
INSERT INTO
   MENU_CATEGORY
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      'Drinks',
      'Drinks',
      3
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      1,
      (select
         id
      from
         MENU_CATEGORY
      where
         name = 'Drinks'),
         (select
            id
         from
            MENU_ITEM
         where
            name = 'Hangover Buttermilk')
   )
;
INSERT INTO
   MENU_CATEGORY_ITEM
VALUES
   (
      nextval('HIBERNATE_SEQUENCE'),
      1,
      (select
         id
      from
         MENU_CATEGORY
      where
         name = 'Drinks'),
         (select
            id
         from
            MENU_ITEM
         where
            name = 'Mango Lassi')
   )
;

commit;