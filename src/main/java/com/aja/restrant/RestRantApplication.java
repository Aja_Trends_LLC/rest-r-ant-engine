package com.aja.restrant;

import com.aja.restrant.menu.domain.usecase.GetMenuInteractorImpl;
import com.aja.restrant.menu.domain.usecase.MenuPresenter;
import com.aja.restrant.menu.domain.usecase.MenuRepository;
import com.aja.restrant.menu.gateway.MenuRepositoryImpl;
import com.aja.restrant.menu.presenter.MenuPresenterImpl;
import com.aja.restrant.order.domain.OrderPresenter;
import com.aja.restrant.order.domain.OrderRepository;
import com.aja.restrant.order.domain.SubmitOrderInteractorImpl;
import com.aja.restrant.order.gateway.OrderRepositoryImpl;
import com.aja.restrant.order.presenter.OrderPresenterImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = {"com.aja.restrant.*"})
@EnableJpaRepositories(basePackages = {"com.aja.restrant.*"})
@ComponentScan
public class RestRantApplication {
  public static void main(String[] args) {
    SpringApplication.run(RestRantApplication.class, args);
  }

  @Bean
  public MenuRepository menuRepository() {
    return new MenuRepositoryImpl();
  }

  @Bean
  public MenuPresenter menuPresenter() {
    return new MenuPresenterImpl();
  }

  @Bean
  public OrderRepository orderRepository() {
    return new OrderRepositoryImpl();
  }

  @Bean
  public OrderPresenter orderPresenter() {
    return new OrderPresenterImpl();
  }

  @Bean
  public GetMenuInteractorImpl getMenuInteractor(final MenuRepository menuRepository,
                                                 final MenuPresenter menuPresenter) {
    return new GetMenuInteractorImpl(menuRepository, menuPresenter);
  }

  @Bean
  public SubmitOrderInteractorImpl getOrderInteractor(final OrderRepository orderRepository,
                                                      final OrderPresenter orderPresenter) {
    return new SubmitOrderInteractorImpl(orderRepository, orderPresenter);
  }

}
