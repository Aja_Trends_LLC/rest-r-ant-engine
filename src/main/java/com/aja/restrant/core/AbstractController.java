package com.aja.restrant.core;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractController {
  private static final String RESPONSE = "response";
  @Autowired
  HttpServletRequest httpServletRequest;

  public Object retrievResponse() {
    return httpServletRequest.getAttribute(RESPONSE);
  }
}
