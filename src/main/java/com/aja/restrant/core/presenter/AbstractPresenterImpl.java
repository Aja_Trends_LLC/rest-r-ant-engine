package com.aja.restrant.core.presenter;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractPresenterImpl {
  private static final String RESPONSE = "response";
  @Autowired
  private HttpServletRequest httpServletRequest;

  public void storeResponse(Object response) {
    httpServletRequest.setAttribute(RESPONSE, response);
  }
}
