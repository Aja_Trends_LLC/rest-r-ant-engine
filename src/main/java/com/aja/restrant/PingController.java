package com.aja.restrant;

import java.util.Optional;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {

  private static final String PING_MESSAGE = "Ping: Hi %s!, from SpringBoot, How are you?";
  private static final String PING_MESSAGE_DEFAULT = "Ping: Hi from SpringBoot";

  @RequestMapping({"/ping/{name}", "/ping"})
  public String ping(@PathVariable(required = false) Optional<String> name) {
    String pingMessage = PING_MESSAGE_DEFAULT;
    if (name.isPresent()) {
      pingMessage = String.format(PING_MESSAGE, name.get());
    }
    return pingMessage;
  }
}
