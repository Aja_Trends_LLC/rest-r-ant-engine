package com.aja.restrant.order.domain;

import com.aja.restrant.order.domain.entity.Order;

public class SubmitOrderInteractorImpl implements SubmitOrderInteractor {

  private OrderRepository orderRepository;
  private OrderPresenter orderPresenter;

  public SubmitOrderInteractorImpl(OrderRepository orderRepository, OrderPresenter orderPresenter) {
    this.orderRepository = orderRepository;
    this.orderPresenter = orderPresenter;
  }

  @Override
  public void process(Order orderObj) {
    String confirmationNumber = setOrder(orderObj);
    orderPresenter.showOrder(confirmationNumber);
  }

  private String setOrder(Order order) {
    return orderRepository.setOrder(order);
  }
}
