package com.aja.restrant.order.domain;

import com.aja.restrant.order.domain.entity.Order;
import java.io.IOException;

public interface SubmitOrderInteractor {
  public void process(Order orderObj) throws IOException;
}
