package com.aja.restrant.order.domain.request;

import java.util.ArrayList;
import java.util.List;

public class OrderRequest {
  private List<OrderItemRequest> orderItems = new ArrayList<>();
  private String tipAmount;

  public List<OrderItemRequest> getOrderItems() {
    return orderItems;
  }

  public void setOrderItems(List<OrderItemRequest> orderItems) {
    this.orderItems = orderItems;
  }

  public String getTipAmount() {
    return tipAmount;
  }

  public void setTipAmount(String tipAmount) {
    this.tipAmount = tipAmount;
  }
}
