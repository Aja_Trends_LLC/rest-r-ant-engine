package com.aja.restrant.order.domain;

public interface OrderPresenter {
  public void showOrder(String confirmationNumber);
}
