package com.aja.restrant.order.domain.entity;

import java.io.Serializable;

public class OrderItem implements Serializable {
  private static final long serialVersionUID = 1L;
  private Long orderItemId;
  private Integer itemsCount;

  public Long getOrderItemId() {
    return orderItemId;
  }

  public void setOrderItemId(Long orderItemId) {
    this.orderItemId = orderItemId;
  }

  public Integer getItemsCount() {
    return itemsCount;
  }

  public void setItemsCount(Integer itemsCount) {
    this.itemsCount = itemsCount;
  }
}
