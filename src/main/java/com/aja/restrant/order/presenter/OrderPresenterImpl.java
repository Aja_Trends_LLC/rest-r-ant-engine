package com.aja.restrant.order.presenter;

import com.aja.restrant.core.presenter.AbstractPresenterImpl;
import com.aja.restrant.order.domain.OrderPresenter;

public class OrderPresenterImpl extends AbstractPresenterImpl implements OrderPresenter {
  @Override
  public void showOrder(String confirmationNumber) {
    storeResponse(confirmationNumber);
  }
}
