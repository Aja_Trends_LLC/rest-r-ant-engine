package com.aja.restrant.order;

import com.aja.restrant.core.AbstractController;
import com.aja.restrant.order.domain.SubmitOrderInteractor;
import com.aja.restrant.order.domain.entity.Order;
import com.aja.restrant.order.domain.entity.OrderItem;
import com.aja.restrant.order.domain.request.OrderItemRequest;
import com.aja.restrant.order.domain.request.OrderRequest;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController extends AbstractController {

  @Autowired
  private SubmitOrderInteractor submitOrderInteractor;

  @CrossOrigin
  @PostMapping("/setorder")
  public @ResponseBody
  Object setOrder(@RequestBody OrderRequest requestBody) throws IOException {
    Order order = convertToRequestModel(requestBody);
    getSubmitOrderInteractor().process(order);
    return retrievResponse();
  }

  private Order convertToRequestModel(OrderRequest requestBody) {
    Order order = new Order();
    if (!requestBody.getOrderItems().isEmpty()) {
      requestBody.getOrderItems().forEach(requestOrderItem ->
          order.getItems().add(getOrderItem(requestOrderItem)));
    }
    order.setTip(requestBody.getTipAmount());
    return order;
  }

  private OrderItem getOrderItem(OrderItemRequest requestOrderItem) {
    OrderItem orderItem = new OrderItem();
    orderItem.setOrderItemId(requestOrderItem.getItemId());
    orderItem.setItemsCount(requestOrderItem.getNumberOfItems());
    return orderItem;
  }

  public SubmitOrderInteractor getSubmitOrderInteractor() {
    return submitOrderInteractor;
  }

  public void setSubmitOrderInteractor(SubmitOrderInteractor submitOrderInteractor) {
    this.submitOrderInteractor = submitOrderInteractor;
  }
}
