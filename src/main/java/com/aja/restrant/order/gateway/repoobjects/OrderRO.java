package com.aja.restrant.order.gateway.repoobjects;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity(name = "order_table")
public class OrderRO {
  private Long id;
  private BigDecimal tipAmount;
  private Set<OrderItemRO> orderItems = new HashSet<>();

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "order_id")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public BigDecimal getTipAmount() {
    return tipAmount;
  }

  public void setTipAmount(BigDecimal tipAmount) {
    this.tipAmount = tipAmount;
  }

  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name = "order_id")
  public Set<OrderItemRO> getOrderItems() {
    return orderItems;
  }

  public void setOrderItems(Set<OrderItemRO> orderItems) {
    this.orderItems = orderItems;
  }
}
