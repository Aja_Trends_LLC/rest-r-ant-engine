package com.aja.restrant.order.gateway.repoobjects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "order_item")
public class OrderItemRO {
  private Long id;
  private Long itemId;
  private Integer itemCounts;
  private OrderRO orderRO;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public Integer getItemCounts() {
    return itemCounts;
  }

  public void setItemCounts(Integer itemCounts) {
    this.itemCounts = itemCounts;
  }

  @ManyToOne
  @JoinColumn(name = "order_id")
  public OrderRO getOrderRO() {
    return orderRO;
  }

  public void setOrderRO(OrderRO orderRO) {
    this.orderRO = orderRO;
  }

}
