package com.aja.restrant.order.gateway;

import com.aja.restrant.order.domain.OrderRepository;
import com.aja.restrant.order.domain.entity.Order;
import com.aja.restrant.order.domain.entity.OrderItem;
import com.aja.restrant.order.gateway.repoobjects.OrderItemRO;
import com.aja.restrant.order.gateway.repoobjects.OrderRO;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

  @Autowired
  private OrderRepo orderRepo;

  @Override
  public String setOrder(Order order) {
    OrderRO orderRO = getOrderRO(order);
    orderRO = orderRepo.save(orderRO);
    return orderRO.getId().toString();
  }

  private OrderRO getOrderRO(Order order) {
    OrderRO orderRO = new OrderRO();
    orderRO.setTipAmount(new BigDecimal(order.getTip()));
    orderRO.setOrderItems(getOrderItemsRO(order.getItems()));
    return orderRO;
  }

  private Set<OrderItemRO> getOrderItemsRO(List<OrderItem> orderItems) {
    Set<OrderItemRO> orderItemROs = new HashSet<>();
    orderItems.forEach(
        orderItem -> orderItemROs.add(getOrderItemRO(orderItem))
    );
    return orderItemROs;
  }

  private OrderItemRO getOrderItemRO(OrderItem orderItem) {
    OrderItemRO orderItemRO = new OrderItemRO();
    orderItemRO.setItemId(orderItem.getOrderItemId());
    orderItemRO.setItemCounts(orderItem.getItemsCount());
    return orderItemRO;
  }
}
