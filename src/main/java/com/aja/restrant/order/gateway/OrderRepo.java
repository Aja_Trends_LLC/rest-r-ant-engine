package com.aja.restrant.order.gateway;

import com.aja.restrant.order.gateway.repoobjects.OrderRO;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepo extends CrudRepository<OrderRO,Long> {
}
