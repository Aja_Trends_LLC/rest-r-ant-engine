package com.aja.restrant.menu.gateway;

import com.aja.restrant.menu.gateway.repoobjects.MenuCategoryRO;

import org.springframework.data.repository.CrudRepository;

public interface MenuCategoryRepo extends CrudRepository<MenuCategoryRO, Long> {
  MenuCategoryRO findByCategoryName(String name);
}
