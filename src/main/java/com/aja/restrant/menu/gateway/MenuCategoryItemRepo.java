package com.aja.restrant.menu.gateway;

import com.aja.restrant.menu.gateway.repoobjects.MenuCategoryItemRO;

import org.springframework.data.repository.CrudRepository;

public interface MenuCategoryItemRepo extends CrudRepository<MenuCategoryItemRO, Long> {
}
