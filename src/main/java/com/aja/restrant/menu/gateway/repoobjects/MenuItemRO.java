package com.aja.restrant.menu.gateway.repoobjects;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = "menu_item")
public class MenuItemRO {
  private Long id;
  private String itemName;
  private String itemDesc;
  private BigDecimal itemPrice;
  private Set<MenuCategoryItemRO> menuCategoryItems = new HashSet<>();

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Column(name = "name")
  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  @Column(name = "description")
  public String getItemDesc() {
    return itemDesc;
  }

  public void setItemDesc(String itemDesc) {
    this.itemDesc = itemDesc;
  }

  @Column(name = "price")
  public BigDecimal getItemPrice() {
    return itemPrice;
  }

  public void setItemPrice(BigDecimal itemPrice) {
    this.itemPrice = itemPrice;
  }

  @OneToMany(mappedBy = "menuItem")
  public Set<MenuCategoryItemRO> getMenuCategoryItems() {
    return menuCategoryItems;
  }

  public void setMenuCategoryItems(Set<MenuCategoryItemRO> menuCategoryItems) {
    this.menuCategoryItems = menuCategoryItems;
  }
}
