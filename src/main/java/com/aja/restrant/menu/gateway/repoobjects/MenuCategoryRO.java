package com.aja.restrant.menu.gateway.repoobjects;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = "menu_category")
public class MenuCategoryRO {
  private Long id;
  private String categoryName;
  private String categoryDesc;
  private Integer rank;
  private Set<MenuCategoryItemRO> menuCategoryItems = new HashSet<>();

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Column(name = "name")
  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  @Column(name = "description")
  public String getCategoryDesc() {
    return categoryDesc;
  }

  public void setCategoryDesc(String categoryDesc) {
    this.categoryDesc = categoryDesc;
  }

  public Integer getRank() {
    return rank;
  }

  public void setRank(Integer rank) {
    this.rank = rank;
  }

  @OneToMany(mappedBy = "menuCategory")
  public Set<MenuCategoryItemRO> getMenuCategoryItems() {
    return menuCategoryItems;
  }

  public void setMenuCategoryItems(Set<MenuCategoryItemRO> menuCategoryItems) {
    this.menuCategoryItems = menuCategoryItems;
  }

}
