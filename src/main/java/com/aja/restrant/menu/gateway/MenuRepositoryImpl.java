package com.aja.restrant.menu.gateway;

import com.aja.restrant.menu.domain.entity.MenuCategory;
import com.aja.restrant.menu.domain.entity.MenuItem;
import com.aja.restrant.menu.domain.entity.NameAndDesc;
import com.aja.restrant.menu.domain.usecase.MenuRepository;
import com.aja.restrant.menu.gateway.repoobjects.MenuCategoryItemRO;
import com.aja.restrant.menu.gateway.repoobjects.MenuCategoryRO;
import com.aja.restrant.menu.gateway.repoobjects.MenuItemRO;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MenuRepositoryImpl implements MenuRepository {

  @Autowired
  private MenuCategoryRepo menuCategoryRepo;

  @Override
  public List<MenuCategory> getMenu() {
    return getMenuCategories();
  }

  private List<MenuCategory> getMenuCategories() {
    List<MenuCategory> menuCategories = new ArrayList<>();
    Iterable<MenuCategoryRO> menuCategoryROs = menuCategoryRepo.findAll();
    menuCategoryROs.forEach(
        menuCategoryRO -> menuCategories.add(getMenuCategory(menuCategoryRO))
    );
    return menuCategories;
  }

  private MenuCategory getMenuCategory(MenuCategoryRO menuCategoryRO) {
    MenuCategory menuCategory = new MenuCategory();
    menuCategory.setId(menuCategoryRO.getId());
    menuCategory.setRank(menuCategoryRO.getRank());
    menuCategory.setNameAndDesc(getCatNameAndDesc(menuCategoryRO));
    menuCategory.setMenuItems(getMenuItems(menuCategoryRO.getMenuCategoryItems()));
    return menuCategory;
  }

  private NameAndDesc getCatNameAndDesc(MenuCategoryRO menuCategoryRO) {
    NameAndDesc nameAndDesc = new NameAndDesc();
    nameAndDesc.setName(menuCategoryRO.getCategoryName());
    nameAndDesc.setDesc(menuCategoryRO.getCategoryDesc());
    return nameAndDesc;
  }

  private List<MenuItem> getMenuItems(Set<MenuCategoryItemRO> menuCategoryItems) {
    List<MenuItem> menuItems = new ArrayList<>();
    menuCategoryItems.forEach(
        menuCategoryItemRO -> menuItems.add(getMenuItem(menuCategoryItemRO))
    );
    return menuItems;
  }

  private MenuItem getMenuItem(MenuCategoryItemRO menuCategoryItemRO) {
    MenuItem menuItem = new MenuItem();
    menuItem.setId(menuCategoryItemRO.getMenuItem().getId());
    menuItem.setRank(menuCategoryItemRO.getMenuItemRank());
    menuItem.setNameAndDesc(getMenuItemNameAndDesc(menuCategoryItemRO.getMenuItem()));
    menuItem.setPrice(menuCategoryItemRO.getMenuItem().getItemPrice());
    return menuItem;
  }

  private NameAndDesc getMenuItemNameAndDesc(MenuItemRO menuItemRO) {
    NameAndDesc nameAndDesc = new NameAndDesc();
    nameAndDesc.setName(menuItemRO.getItemName());
    nameAndDesc.setDesc(menuItemRO.getItemDesc());
    return nameAndDesc;
  }

}
