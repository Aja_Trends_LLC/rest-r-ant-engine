package com.aja.restrant.menu.gateway.repoobjects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "menu_category_item")
public class MenuCategoryItemRO {
  private Long id;
  private MenuCategoryRO menuCategory;
  private MenuItemRO menuItem;
  private Integer menuItemRank;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "menucategory_id")
  public MenuCategoryRO getMenuCategory() {
    return menuCategory;
  }

  public void setMenuCategory(MenuCategoryRO menuCategory) {
    this.menuCategory = menuCategory;
  }

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "menuitem_id")
  public MenuItemRO getMenuItem() {
    return menuItem;
  }

  public void setMenuItem(MenuItemRO menuItem) {
    this.menuItem = menuItem;
  }

  public Integer getMenuItemRank() {
    return menuItemRank;
  }

  public void setMenuItemRank(Integer menuItemRank) {
    this.menuItemRank = menuItemRank;
  }
}
