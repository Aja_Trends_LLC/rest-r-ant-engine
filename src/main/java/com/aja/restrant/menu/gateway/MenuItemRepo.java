package com.aja.restrant.menu.gateway;

import com.aja.restrant.menu.gateway.repoobjects.MenuItemRO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuItemRepo extends CrudRepository<MenuItemRO, Long> {
  MenuItemRO findByItemName(String name);
}
