package com.aja.restrant.menu.domain.entity;

import java.io.Serializable;
import java.util.List;

public class Menu implements Serializable {
  private static final long serialVersionUID = 1L;

  private List<MenuCategory> categories;

  public List<MenuCategory> getCategories() {
    return categories;
  }

  public void setCategories(List<MenuCategory> categories) {
    this.categories = categories;
  }

}
