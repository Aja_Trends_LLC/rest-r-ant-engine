package com.aja.restrant.menu.domain.usecase;

import com.aja.restrant.menu.domain.entity.MenuCategory;

import java.util.List;

public interface MenuPresenter {
  public void showMenu(List<MenuCategory> menuCategories);
}
