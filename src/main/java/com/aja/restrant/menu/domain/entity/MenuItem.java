package com.aja.restrant.menu.domain.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class MenuItem extends OrderedEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  private NameAndDesc nameAndDesc;
  private BigDecimal price;

  public NameAndDesc getNameAndDesc() {
    return nameAndDesc;
  }

  public void setNameAndDesc(NameAndDesc nameAndDesc) {
    this.nameAndDesc = nameAndDesc;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }
}
