package com.aja.restrant.menu.domain.entity;

import java.io.Serializable;
import java.util.List;

public class MenuCategory extends OrderedEntity implements Serializable {
  private static final long serialVersionUID = 1L;

  private List<MenuItem> menuItems;
  private NameAndDesc nameAndDesc;

  public List<MenuItem> getMenuItems() {
    return menuItems;
  }

  public void setMenuItems(List<MenuItem> menuItems) {
    this.menuItems = menuItems;
  }

  public NameAndDesc getNameAndDesc() {
    return nameAndDesc;
  }

  public void setNameAndDesc(NameAndDesc nameAndDesc) {
    this.nameAndDesc = nameAndDesc;
  }
}
