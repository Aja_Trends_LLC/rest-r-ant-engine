package com.aja.restrant.menu.domain.usecase;

import com.aja.restrant.menu.domain.entity.MenuCategory;

import java.util.List;

public class GetMenuInteractorImpl implements GetMenuInteractor {
  private MenuRepository menuRepository;
  private MenuPresenter menuPresenter;

  public GetMenuInteractorImpl(MenuRepository menuRepository, MenuPresenter menuPresenter) {
    this.menuRepository = menuRepository;
    this.menuPresenter = menuPresenter;
  }

  public void process() {
    List<MenuCategory> menuCategories = menuRepository.getMenu();
    menuPresenter.showMenu(menuCategories);
  }

}
