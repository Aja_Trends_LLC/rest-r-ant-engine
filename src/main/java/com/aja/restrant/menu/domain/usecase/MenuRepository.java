package com.aja.restrant.menu.domain.usecase;

import com.aja.restrant.menu.domain.entity.MenuCategory;

import java.util.List;

public interface MenuRepository {
  List<MenuCategory> getMenu();
}
