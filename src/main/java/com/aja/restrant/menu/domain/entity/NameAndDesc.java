package com.aja.restrant.menu.domain.entity;

import java.io.Serializable;

public class NameAndDesc implements Serializable {
  private String name;
  private String desc;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }
}
