package com.aja.restrant.menu.domain.usecase;

public interface GetMenuInteractor {
  public void process();
}
