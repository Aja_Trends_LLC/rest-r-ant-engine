package com.aja.restrant.menu.domain.entity;

import java.io.Serializable;

public class OrderedEntity extends Entity implements Serializable {
  private static final long serialVersionUID = 1L;
  private Integer rank;

  public Integer getRank() {
    return rank;
  }

  public void setRank(Integer rank) {
    this.rank = rank;
  }
}
