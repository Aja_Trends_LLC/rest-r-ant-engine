package com.aja.restrant.menu.presenter.model;

import java.util.List;

public class MenuCategoryVM {
  private List<MenuItemVM> menuItems;
  private String categoryName;
  private String categoryDesc;

  public List<MenuItemVM> getMenuItems() {
    return menuItems;
  }

  public void setMenuItems(List<MenuItemVM> menuItems) {
    this.menuItems = menuItems;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public String getCategoryDesc() {
    return categoryDesc;
  }

  public void setCategoryDesc(String categoryDesc) {
    this.categoryDesc = categoryDesc;
  }
}
