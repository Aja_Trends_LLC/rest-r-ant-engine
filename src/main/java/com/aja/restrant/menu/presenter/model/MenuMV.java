package com.aja.restrant.menu.presenter.model;

import java.util.List;

public class MenuMV {
  private List<MenuCategoryVM> categories;

  public List<MenuCategoryVM> getCategories() {
    return categories;
  }

  public void setCategories(List<MenuCategoryVM> categories) {
    this.categories = categories;
  }

}
