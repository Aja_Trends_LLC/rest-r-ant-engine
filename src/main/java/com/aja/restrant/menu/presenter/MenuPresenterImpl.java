package com.aja.restrant.menu.presenter;

import com.aja.restrant.core.presenter.AbstractPresenterImpl;
import com.aja.restrant.menu.domain.entity.MenuCategory;
import com.aja.restrant.menu.domain.entity.MenuItem;
import com.aja.restrant.menu.domain.usecase.MenuPresenter;
import com.aja.restrant.menu.presenter.model.MenuCategoryVM;
import com.aja.restrant.menu.presenter.model.MenuItemVM;
import com.aja.restrant.menu.presenter.model.MenuMV;
import java.util.ArrayList;
import java.util.List;


public class MenuPresenterImpl  extends AbstractPresenterImpl implements MenuPresenter {

  @Override
  public void showMenu(List<MenuCategory> menuCategories) {
     MenuMV menuMV = getMenuMV(menuCategories);
      storeResponse(menuMV);
  }

  private MenuMV getMenuMV(List<MenuCategory> menuCategories) {
    MenuMV menuMV = new MenuMV();
    menuMV.setCategories(getCategoriesMV(menuCategories));
    return menuMV;
  }

  private List<MenuCategoryVM> getCategoriesMV(List<MenuCategory> menuCategories) {
    List<MenuCategoryVM> categorysMV = new ArrayList<>();
    menuCategories.forEach(menuCategory ->
        categorysMV.add(getMenuCategoryMV(menuCategory))
    );
    return categorysMV;
  }

  private MenuCategoryVM getMenuCategoryMV(MenuCategory menuCategory) {
    MenuCategoryVM menuCategoryVM = new MenuCategoryVM();
    menuCategoryVM.setCategoryName(menuCategory.getNameAndDesc().getName());
    menuCategoryVM.setCategoryDesc(menuCategory.getNameAndDesc().getDesc());
    List<MenuItemVM> menuItemVMs = getMenuItemsMV(menuCategory.getMenuItems());
    menuCategoryVM.setMenuItems(menuItemVMs);
    return menuCategoryVM;
  }

  private List<MenuItemVM> getMenuItemsMV(List<MenuItem> menuItems) {
    List<MenuItemVM> menuItemsMV = new ArrayList<>();
    menuItems.forEach(menuItem ->
        menuItemsMV.add(getMenuItemMV(menuItem))
    );
    return menuItemsMV;
  }

  private MenuItemVM getMenuItemMV(MenuItem menuItem) {
    MenuItemVM menuItemsMV = new MenuItemVM();
    menuItemsMV.setItemName(menuItem.getNameAndDesc().getName());
    menuItemsMV.setItemDesc(menuItem.getNameAndDesc().getDesc());
    menuItemsMV.setItemPrice(menuItem.getPrice());
    menuItemsMV.setItemId(menuItem.getId());
    return menuItemsMV;
  }
}
