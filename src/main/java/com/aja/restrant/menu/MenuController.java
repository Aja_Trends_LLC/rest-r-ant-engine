package com.aja.restrant.menu;

import com.aja.restrant.menu.domain.usecase.GetMenuInteractor;

import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@Slf4j
public class MenuController {
  private static final String RESPONSE = "response";
  @Autowired
  HttpServletRequest httpServletRequest;

  @Autowired
  private GetMenuInteractor getMenuInteractor;

  @CrossOrigin
  @GetMapping("/showmenu")
  public @ResponseBody Object showMenu() {
    log.debug("Calling process on interactor");
     getMenuInteractor.process();
     return retrieveResponse();
  }

  private Object retrieveResponse() {
    return httpServletRequest.getAttribute(RESPONSE);
  }
}
